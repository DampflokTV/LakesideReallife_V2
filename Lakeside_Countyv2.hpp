class Applewood
{
	name="New Lakewood";
	position[]={8659.78,6975.07};
	type="NameVillage";
	
	radiusA=156.92;
	radiusB=159.26;
	angle=0.000;
};
class Oldtown
{
	name="Old Town";
	position[]={8201.47,6135.73};
	type="NameVillage";
	
	radiusA=270.80;
	radiusB=252.50;
	angle=25.000;
};
class PaloHeights
{
	name="Palomina Heights";
	position[]={7956.41,6587.69};
	type="ViewPoint";
	
	radiusA=245.19;
	radiusB=118.02;
	angle=0.000;
};
class Smallville
{
	name="Smallville";
	position[]={8865.13,8255.66};
	type="NameCity";
	
	radiusA=223.66;
	radiusB=114.18;
	angle=0.000;
};
class sanmarcos
{
	name="Lakside Air National Guard Base";
	position[]={9433.19,9106.24};
	type="Airport";
	
	radiusA=829.11;
	radiusB=707.59;
	angle=320.000;
};
class Farm1
{
	name="Lakeside Water Authority";
	position[]={9471.05,9845.04};
	type="NameMarine";
	
	radiusA=507.02;
	radiusB=352.90;
	angle=0.000;
};
class projects
{
	name="The Projects";
	position[]={8244.64,6336.36};
	type="NameLocal";
	
	radiusA=117.81;
	radiusB=87.24;
	angle=310.000;
};
class marina
{
	name="Lakeside Marina";
	position[]={8142.34,5987.81};
	type="NameLocal";
	
	radiusA=257.08;
	radiusB=195.78;
	angle=0.000;
};
class bighorn
{
	name="Big Horn Peak";
	position[]={9257.50,1665.31};
	type="Hill";
	
	radiusA=187.06;
	radiusB=161.93;
	angle=0.000;
};
class littlehorn
{
	name="Little Horn Peak";
	position[]={8640.47,1675.08};
	type="Hill";
	
	radiusA=248.48;
	radiusB=214.98;
	angle=0.000;
};
class deadhorse
{
	name="Dead Horse Pass";
	position[]={8957.50,1697.66};
	type="ViewPoint";
	
	radiusA=253.73;
	radiusB=355.58;
	angle=0.000;
};
class EZLNHQ
{
	name="San Bilogen";
	position[]={4873.74,859.57};
	type="NameVillage";
	
	radiusA=500.00;
	radiusB=500.00;
	angle=0.000;
};
class santamaria
{
	name="Santa Maria";
	position[]={3765.07,2116.09};
	type="NameVillage";
	
	radiusA=258.80;
	radiusB=226.01;
	angle=0.000;
};
class border2
{
	name="Shelbyville";
	position[]={3901.21,2737.84};
	type="NameVillage";
	
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};
class heights2
{
	name="Jefferson Hill";
	position[]={2844.32,5672.43};
	type="NameLocal";
	
	radiusA=481.63;
	radiusB=394.36;
	angle=0.000;
};
class airport1
{
	name="Los Diablos International Airport";
	position[]={2551.67,7643.43};
	type="Airport";
	
	radiusA=1197.19;
	radiusB=263.56;
	angle=0.000;
};
class hospital1
{
	name="Lakeside Hospital";
	position[]={8481.18,6447.86};
	type="NameLocal";
	
	radiusA=81.98;
	radiusB=93.68;
	angle=0.000;
};
class ranch
{
	name="Hoover Ranch";
	position[]={9111.81,8227.01};
	type="VegetationVineyard";
	
	radiusA=103.59;
	radiusB=75.31;
	angle=0.000;
};
class pecos
{
	name="Pecos Parachute School";
	position[]={519.81,1240.61};
	type="Airport";
	
	radiusA=723.13;
	radiusB=594.69;
	angle=0.000;
};
class fortlake
{
	name="Fort Walden National Historic Site";
	position[]={6488.34,4131.10};
	type="ViewPoint";
	
	radiusA=304.81;
	radiusB=290.34;
	angle=0.000;
};
class hacker
{
	name="Hacker Gulch";
	position[]={5642.64,3997.25};
	type="NameMarine";
	
	radiusA=219.56;
	radiusB=207.94;
	angle=0.000;
};
class manzanita
{
	name="Manzanita Creek";
	position[]={5123.53,2376.61};
	type="NameMarine";
	
	radiusA=387.47;
	radiusB=177.72;
	angle=0.000;
};
class carberry
{
	name="Carberry Creek";
	position[]={4483.97,2142.59};
	type="NameMarine";
	
	radiusA=218.02;
	radiusB=170.48;
	angle=0.000;
};
class spring
{
	name="Spring Gulch";
	position[]={8365.89,3770.29};
	type="NameMarine";
	
	radiusA=635.69;
	radiusB=236.11;
	angle=0.000;
};
class grouse
{
	name="Grouse Creek";
	position[]={6911.71,5302.70};
	type="NameMarine";
	
	radiusA=452.05;
	radiusB=351.14;
	angle=0.000;
};
class french
{
	name="French Gulch";
	position[]={8635.45,5140.94};
	type="NameMarine";
	
	radiusA=339.03;
	radiusB=232.07;
	angle=0.000;
};
class kanaka
{
	name="Kanaka Gulch";
	position[]={7690.02,7575.19};
	type="RockArea";
	
	radiusA=561.02;
	radiusB=419.75;
	angle=0.000;
};
class black
{
	name="Black Gulch";
	position[]={8299.31,9172.57};
	type="RockArea";
	
	radiusA=704.30;
	radiusB=304.72;
	angle=0.000;
};
class mule
{
	name="Mule Creek";
	position[]={9931.77,9647.63};
	type="RockArea";
	
	radiusA=384.69;
	radiusB=293.25;
	angle=0.000;
};
class kinney
{
	name="Kinney Creek";
	position[]={8626.34,9970.83};
	type="RockArea";
	
	radiusA=643.25;
	radiusB=428.83;
	angle=0.000;
};
class carberryg
{
	name="Carberry Gulch";
	position[]={3609.03,4581.13};
	type="RockArea";
	
	radiusA=882.89;
	radiusB=817.30;
	angle=0.000;
};
class bh
{
	name="Brotherhood Heights";
	position[]={1844.34,8786.60};
	type="Hill";
	
	radiusA=518.64;
	radiusB=407.64;
	angle=0.000;
};
class deathroad
{
	name="The Road of Death";
	position[]={5529.20,8019.12};
	type="NameLocal";
	
	radiusA=462.89;
	radiusB=320.30;
	angle=320.000;
};
class ACDCLane
{
	name="ACDC Lane";
	position[]={8607.73,6475.06};
	type="NameLocal";
	
	radiusA=200.00;
	radiusB=25.00;
	angle=280.000;
};
class lincoln
{
	name="Lincoln Rd";
	position[]={8454.11,6500.30};
	type="NameLocal";
	
	radiusA=200.00;
	radiusB=25.00;
	angle=280.000;
};
class ParkAve
{
	name="Park Ave";
	position[]={8662.92,6816.49};
	type="NameLocal";
	
	radiusA=200.00;
	radiusB=25.00;
	angle=10.000;
};
class 9st
{
	name="9th St";
	position[]={8529.62,6714.64};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=10.000;
};
class 8th
{
	name="8th St";
	position[]={8506.08,6597.50};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=10.000;
};
class 7th
{
	name="7th St";
	position[]={8566.87,6460.31};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=10.000;
};
class 6th
{
	name="6th St";
	position[]={8472.72,6408.79};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=10.000;
};
class 5th
{
	name="5th St";
	position[]={8461.22,6318.13};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=10.000;
};
class Patton
{
	name="Patton Rd";
	position[]={8268.77,6310.76};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=310.000;
};
class 4th
{
	name="4th St";
	position[]={8284.26,6384.06};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=40.000;
};
class 3rd
{
	name="3rd St";
	position[]={8214.17,6295.45};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=40.000;
};
class 2nd
{
	name="2nd St";
	position[]={8177.14,6246.52};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=40.000;
};
class 1st
{
	name="1st St";
	position[]={8115.63,6140.71};
	type="NameLocal";
	
	radiusA=100.00;
	radiusB=15.00;
	angle=25.000;
};
class pb
{
	name="EMS/Fire Station";
	position[]={8800.96,6724.21};
	type="NameLocal";
	
	radiusA=63.13;
	radiusB=66.63;
	angle=0.000;
};
class heli
{
	name="Lakeside Heliport";
	position[]={8246.45,5902.90};
	type="Airport";
	
	radiusA=205.00;
	radiusB=143.83;
	angle=0.000;
};
class Bedford
{
	name="New Bedford";
	position[]={6685.13,3822.17};
	type="NameVillage";
	
	radiusA=665.14;
	radiusB=227.31;
	angle=0.000;
};
class Devils
{
	name="Devil's Sinkhole";
	position[]={498.31,580.31};
	type="RockArea";
	
	radiusA=181.14;
	radiusB=112.74;
	angle=0.000;
};
class DefaultKeyPoint1
{
	name="Morrison Town";
	position[]={5244.83,2922.42};
	type="NameVillage";
	
	radiusA=191.14;
	radiusB=141.13;
	angle=0.000;
};
class DefaultKeyPoint2
{
	name="Klein Los Diablos";
	position[]={3673.79,7231.64};
	type="NameCity";
	
	radiusA=78.29;
	radiusB=57.81;
	angle=0.000;
};
class DefaultKeyPoint6
{
	name="East Bend";
	position[]={8709.50,4656.33};
	type="NameCity";
	
	radiusA=100.00;
	radiusB=50.00;
	angle=0.000;
};
class DefaultKeyPoint7
{
	name="Tempel Town";
	position[]={8905.45,2695.30};
	type="NameCity";
	
	radiusA=100.00;
	radiusB=100.00;
	angle=0.000;
};
class DefaultKeyPoint10
{
	name="DefaultKeyPoint10";
	position[]={7712.41,5545.93};
	type="Hill";
	
	radiusA=2.27;
	radiusB=2.27;
	angle=0.000;
};
class springcliff
{
	name="Springfield Cliffs";
	position[]={8096.42,5293.14};
	type="NameVillage";
	
	radiusA=585.23;
	radiusB=389.48;
	angle=0.000;
};
class DefaultKeyPoint13
{
	name="#";
	position[]={7688.52,6881.84};
	type="Hill";
	
	radiusA=5.00;
	radiusB=5.00;
	angle=0.000;
};
class DefaultKeyPoint14
{
	name="#";
	position[]={7266.38,2182.49};
	type="Hill";
	
	radiusA=5.00;
	radiusB=5.00;
	angle=0.000;
};
class DefaultKeyPoint15
{
	name="#";
	position[]={2057.42,7060.86};
	type="Hill";
	
	radiusA=5.00;
	radiusB=5.00;
	angle=0.000;
};
class DefaultKeyPoint16
{
	name="#";
	position[]={5810.66,8463.77};
	type="Hill";
	
	radiusA=5.00;
	radiusB=5.00;
	angle=0.000;
};
class DefaultKeyPoint17
{
	name="Klein Morrison Town";
	position[]={4614.68,2433.59};
	type="NameCity";
	
	radiusA=100.00;
	radiusB=100.00;
	angle=0.000;
};
class NewHaven
{
	name="New Haven";
	position[]={5873.44,8235.90};
	type="NameCity";
	
	radiusA=524.69;
	radiusB=318.85;
	angle=0.000;
};
class Knoxville
{
	name="Knoxville";
	position[]={4089.75,6464.52};
	type="NameCity";
	
	radiusA=665.00;
	radiusB=227.00;
	angle=0.000;
};
class SanCristobal
{
	name="San Cristobal";
	position[]={5061.20,1176.18};
	type="NameVillage";
	
	radiusA=665.00;
	radiusB=275.33;
	angle=0.000;
};
